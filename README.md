README

This is an internal application for my organization, Mobile Developers of Berkeley. It allows users to check into meetings, request absences, and view upcoming events.

This project was built using Android Studio and uses Firebase and Google Calendar API.

Contact slamba10@berkeley.edu for any questions.