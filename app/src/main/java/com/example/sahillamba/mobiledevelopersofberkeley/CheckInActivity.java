package com.example.sahillamba.mobiledevelopersofberkeley;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.util.Value;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class CheckInActivity extends AppCompatActivity {

    EditText code;
    private String mUsername;
    String codeOfTheDay;

    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.hide();
        Intent intent = getIntent();
        mUsername = intent.getStringExtra("username");
        code = (EditText) findViewById(R.id.code);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        setupAnimations();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        ValueEventListener codeWordListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object codeWrapper = dataSnapshot.child("allowcheckin/codeWord").getValue();
                codeOfTheDay = (String) codeWrapper;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(codeWordListener);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View focus = null;
                String myCode = code.getText().toString();
                if (myCode.equals(codeOfTheDay)) {
                    //write to the user table that user is checked in
                    mDatabase.child("users").child(mUsername).child("isCheckedIn").setValue(true);
                    Intent intent = new Intent(CheckInActivity.this, MainActivity.class);
                    intent.putExtra("username", mUsername);
                    startActivity(intent);
                }
                else {
                    code.setError("Wrong code. Please try again.");
                    focus = code;
                    focus.requestFocus();
                }


            }
        });
    }

    private void setupAnimations() {
        Slide slide = new Slide();
        slide.setDuration(1000);
        getWindow().setEnterTransition(slide);
    }

}
