package com.example.sahillamba.mobiledevelopersofberkeley;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sahillamba on 6/7/16.
 */
@IgnoreExtraProperties
public class AllowCheckIn {
    private boolean allowCheckIn;
    private String codeWord;

    public AllowCheckIn(){}

    public AllowCheckIn(boolean f, String mCodeWord) {
        allowCheckIn = f;
        codeWord = mCodeWord;
    }

    @Exclude
    public boolean getAllow() {
        return allowCheckIn;
    }

    @Exclude
    public String getCodeWord() {
        return codeWord;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> aciMap = new HashMap<>();
        aciMap.put("allowCheckIn", allowCheckIn);
        aciMap.put("codeWord", codeWord);
        return aciMap;
    }
}
