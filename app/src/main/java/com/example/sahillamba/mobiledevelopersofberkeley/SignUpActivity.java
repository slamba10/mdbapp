package com.example.sahillamba.mobiledevelopersofberkeley;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private Button signUpButton;
    private EditText mName;
    private EditText mUsername;
    private EditText mPassword;
    private EditText mConfirm;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static String TAG = "am i in?";
    private DatabaseReference ref;
    private String mKey;

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void writeNewUser(String name, String email) {
//        String key = ref.child("users").push().getKey();
        User user = new User(email, name);
        int atPosition = email.indexOf("@");
        String username = email.substring(0, atPosition);
//        Map<String, Object> userMap = user.toMap();
//        Map<String, Object> childUpdates = new HashMap<>();
//        childUpdates.put("/users/" + key, userMap);
//        ref.updateChildren(childUpdates);
//        return key;
        ref.child("users").child(username).setValue(user);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ActionBar bar = getSupportActionBar();
        bar.hide();

        signUpButton = (Button) findViewById(R.id.sign_up);
        mName = (EditText) findViewById(R.id.name);
        mUsername = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mConfirm = (EditText) findViewById(R.id.confirmpass);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("userID", mKey);
                    startActivity(intent);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
        ref = FirebaseDatabase.getInstance().getReference();
//        ref.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // This method is called once with the initial value and again
//                // whenever data at this location is updated.
//                String value = dataSnapshot.getValue(String.class);
//                Log.d(TAG, "Value is: " + value);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
//            }
//        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String us = mUsername.getText().toString();
                String pass = mPassword.getText().toString();
                String cpass = mConfirm.getText().toString();

                boolean cancel = false;
                View focus = null;
                if (!pass.equals(cpass)) {
                    mPassword.setError("Passwords do not match.");
                    cancel = true;
                    focus = mPassword;
                }
                if (pass.length() < 5) {
                    mPassword.setError("Password too short");
                    cancel = true;
                    focus = mPassword;
                }
                if (!us.contains("@")) {
                    mUsername.setError("Not a valid email.");
                    cancel = true;
                    focus = mUsername;
                }
                if (cancel) {
                    focus.requestFocus();
                }
                else {
                    //Authenticate
                    mAuth.createUserWithEmailAndPassword(us, pass)
                            .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        writeNewUser(mName.getText().toString(), mUsername.getText().toString());

                                    }


                                    // ...
                                }
                            });
                }


            }
        });
    }


}
