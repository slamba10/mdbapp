package com.example.sahillamba.mobiledevelopersofberkeley;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sahillamba on 6/7/16.
 */
@IgnoreExtraProperties
public class User {
//    public String uID;
    public String email;
    public String name;
    public int numHackshopAbsencesLeft;
    public int numMeetingAbsencesLeft;
    public int numUnexcusedLeft;
    public boolean isCheckedIn;
    public boolean excused;

    public User() {

    }

    public User(String mEmail, String mName) {
//        uID = mUid;
        email = mEmail;
        name = mName;
        numHackshopAbsencesLeft = 2;
        numMeetingAbsencesLeft = 2;
        numUnexcusedLeft = 1;
        isCheckedIn = false;
        excused = false;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
//        result.put("uID", uID);
        result.put("email", email);
        result.put("name", name);
        result.put("numHackshopAbsencesLeft", numHackshopAbsencesLeft);
        result.put("numMeetingAbsencesLeft", numMeetingAbsencesLeft);
        result.put("numUnexcusedLeft", numUnexcusedLeft);
        result.put("isCheckedIn", isCheckedIn);
        result.put("excused", excused);
        return result;
    }

    @Exclude
    public boolean isCheckedIn() {
        return isCheckedIn;
    }

}
