package com.example.sahillamba.mobiledevelopersofberkeley;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sahillamba on 6/17/16.
 */
@IgnoreExtraProperties
public class Request {

    private String reason;
    private String date;
    private String requestedBy;
    private boolean approved;
    private boolean pending;

    public Request() {

    }
    public Request(String mReason, String mDate, String mRequestBy) {
        reason = mReason;
        date = mDate;
        requestedBy = mRequestBy;
        approved = false;
        pending = true;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap result = new HashMap<>();
        result.put("date", date);
        result.put("reason", reason);
        result.put("requestedBy", requestedBy);
        result.put("approved", approved);
        result.put("pending", pending);
        return result;
    }
}
