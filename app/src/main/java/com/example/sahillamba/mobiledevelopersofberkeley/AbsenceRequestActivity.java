package com.example.sahillamba.mobiledevelopersofberkeley;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.sergiocasero.revealfab.RevealFAB;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AbsenceRequestActivity extends AppCompatActivity {

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;
    private EditText reasonView;
    private String mUsername;
    RevealFAB revealFAB;

    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absence_request);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.hide();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent2 = getIntent();
        mUsername = intent2.getStringExtra("username");

        setupAnimations();

        reasonView = (EditText) findViewById(R.id.reason);


        dateView = (TextView) findViewById(R.id.date);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month, day);
        revealFAB = (RevealFAB) findViewById(R.id.reveal_fabulous);
        Intent intent = new Intent(AbsenceRequestActivity.this, AbsenceActivity.class);
        intent.putExtra("username", mUsername);
        revealFAB.setIntent(intent);
        revealFAB.setOnClickListener(new RevealFAB.OnClickListener() {
            @Override
            public void onClick(RevealFAB button, View v) {
                //create request in backend
                String mReason = reasonView.getText().toString();
                String mDate = dateView.getText().toString();
                writeNewRequest(mReason, mDate, mUsername);

                //Go back to MainActivity
                Intent intent = new Intent(AbsenceRequestActivity.this, MainActivity.class);
                intent.putExtra("username", mUsername);
                startActivity(intent);

            }
        });
        //fab.setImageResource(R.drawable.ic_send_24dp);
    }
    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
//        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT)
//                .show();
    }

    private void writeNewRequest(String reason, String date, String requestBy) {
        String key = mDatabase.child("requests").push().getKey();
        Log.d("This is the key:", key);
        Request request = new Request(reason, date, requestBy);
        Map<String, Object> requestMap = request.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/requests/" + key, requestMap);
        mDatabase.updateChildren(childUpdates);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }



    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(month + 1).append("/")
                .append(day).append("/").append(year));
    }

    @Override
    protected void onResume() {
        super.onResume();
        revealFAB.onResume();
    }

    private void setupAnimations() {
        Fade fade = new Fade();
        fade.setDuration(1000);
        getWindow().setEnterTransition(fade);
    }


}
