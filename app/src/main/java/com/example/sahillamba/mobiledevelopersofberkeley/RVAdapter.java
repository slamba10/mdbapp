package com.example.sahillamba.mobiledevelopersofberkeley;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.ramotion.foldingcell.FoldingCell;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sahillamba on 4/6/16.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.EventViewHolder>{

    List<Event> events;
    static RVAdapterListener rvl;
    //GoogleAccountCredential mCred;


    final private String[] MONTHS = {"January","February","March","April","May",
            "June","July","August","September","October","November","December"};



    public RVAdapter(List<Event> events, RVAdapterListener rvl) {
        this.events = events;
        this.rvl = rvl;

    }
    public List<Event> getEvents() {
        return events;
    }


    public interface RVAdapterListener {
        void onButtonClick(View v, int position);

    }
    public static class EventViewHolder extends RecyclerView.ViewHolder {


        FoldingCell foldingCell;
        TextView eventName;
        TextView eventNameFolded;
        TextView eventDate;
        TextView eventDateFolded;
        TextView eventStartTime;
        TextView eventEndTime;
        TextView eventTimeFolded;
        TextView eventLocation;
        TextView eventDescription;
        Button add;


        EventViewHolder(View itemView) {
            super(itemView);
            foldingCell = (FoldingCell)itemView.findViewById(R.id.folding_cell);
            foldingCell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    foldingCell.toggle(false);
                }
            });
            eventName = (TextView)itemView.findViewById(R.id.eventName);
            eventNameFolded = (TextView)itemView.findViewById(R.id.eventNameFolded);
            eventDate = (TextView)itemView.findViewById(R.id.eventDate);
            eventDateFolded = (TextView)itemView.findViewById(R.id.eventDateFolded);
            eventStartTime = (TextView)itemView.findViewById(R.id.eventStartTime);
            eventEndTime = (TextView) itemView.findViewById(R.id.eventEndTime);
            eventTimeFolded = (TextView)itemView.findViewById(R.id.eventTimeFolded);
            eventLocation = (TextView)itemView.findViewById(R.id.eventLoc);
            eventDescription = (TextView)itemView.findViewById(R.id.eventDesc);
            add = (Button)itemView.findViewById(R.id.add);
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rvl.onButtonClick(v, getAdapterPosition());
                }
            });




        }


    }
    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_view, viewGroup, false);
        EventViewHolder pvh = new EventViewHolder(v);
        return pvh;
    }
    @Override
    public void onBindViewHolder(EventViewHolder personViewHolder, final int i) {
        personViewHolder.eventName.setText(events.get(i).getSummary());
        personViewHolder.eventNameFolded.setText(events.get(i).getSummary());
//        Log.d("Event name:", events.get(i).getEventName());
        //personViewHolder.eventDate.setText(events.get(i).getStart().toString());

        DateTime start = events.get(i).getStart().getDateTime();
        String startDate = start.toString().substring(0,10);
        int sy = Integer.parseInt(startDate.substring(0,4));
        int sd = Integer.parseInt(startDate.substring(8,10));
        String sm = MONTHS[Integer.parseInt(startDate.substring(5,7)) - 1];
        String startTime = start.toString().substring(11, 16);


        DateTime end = events.get(i).getEnd().getDateTime();
        String endDate = end.toString().substring(0,10);
        int ey = Integer.parseInt(startDate.substring(0,4));
        int ed = Integer.parseInt(startDate.substring(8,10));
        String em = MONTHS[Integer.parseInt(startDate.substring(5,7)) - 1];
        String endTime = end.toString().substring(11, 16);

        personViewHolder.eventStartTime.setText("Starts: " + startTime);
        personViewHolder.eventEndTime.setText("Ends: " + endTime);
        personViewHolder.eventTimeFolded.setText(startTime);
        String date = sm + " " + sd + ", " + sy;
        personViewHolder.eventDate.setText(date);
        personViewHolder.eventDateFolded.setText(date);
        personViewHolder.eventLocation.setText("Location: " + events.get(i).getLocation());
        personViewHolder.eventDescription.setText("Details: " + events.get(i).getDescription());

    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}