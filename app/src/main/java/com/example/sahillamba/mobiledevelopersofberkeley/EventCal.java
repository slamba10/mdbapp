package com.example.sahillamba.mobiledevelopersofberkeley;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.api.client.util.DateTime;

import java.io.Serializable;

/**
 * Created by sahillamba on 4/7/16.
 */
public class EventCal implements Parcelable {
    String eventName;
//    String eventMonth;
//    int eventDay;
//    int eventYear;
    Serializable start;
    Serializable end;
    //String eventTime;
    String eventDescription;
    String eventLoc;

    public EventCal(String mEventName, Serializable mStart, Serializable mEnd, String desc, String mEventLoc) {
        eventName = mEventName;
//        eventMonth = m;
//        eventDay = d;
//        eventYear = y;
        start = mStart;
        end = mEnd;
        //eventTime = mTime;
        eventDescription = desc;
        eventLoc = mEventLoc;
    }

    protected EventCal(Parcel in) {
        eventName = in.readString();
//        eventMonth = in.readString();
//        eventDay = in.readInt();
//        eventYear = in.readInt();
        start = in.readSerializable();
        end = in.readSerializable();
       // eventTime = in.readString();
        eventDescription = in.readString();
        eventLoc = in.readString();
    }

    public static final Creator<EventCal> CREATOR = new Creator<EventCal>() {
        @Override
        public EventCal createFromParcel(Parcel in) {
            return new EventCal(in);
        }

        @Override
        public EventCal[] newArray(int size) {
            return new EventCal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventName);
//        dest.writeString(eventMonth);
//        dest.writeInt(eventDay);
//        dest.writeInt(eventYear);
        dest.writeSerializable(start);
        dest.writeSerializable(end);
        //dest.writeString(eventTime);
        dest.writeString(eventDescription);
        dest.writeString(eventLoc);
    }

    public String getEventLoc() {
        return eventLoc;
    }

    public String getEventName() {
        return eventName;
    }

    public Serializable getStart() {
        return start;
    }

    public Serializable getEnd() {
        return end;
    }

    public String getEventDescription() {
        return eventDescription;
    }
}