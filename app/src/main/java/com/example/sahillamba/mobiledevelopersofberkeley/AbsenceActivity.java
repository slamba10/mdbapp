package com.example.sahillamba.mobiledevelopersofberkeley;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.sergiocasero.revealfab.RevealFAB;

public class AbsenceActivity extends AppCompatActivity {

    TextView hackshop;
    TextView meeting;
    TextView unexcused;
    RevealFAB revealFAB;
    private DatabaseReference mDatabase;
    private String mUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absence);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();
        bar.hide();

        Intent intent = getIntent();
        mUsername = intent.getStringExtra("username");
        setupWindowAnimations();

        hackshop = (TextView) findViewById(R.id.hackshops);
        meeting = (TextView) findViewById(R.id.meetings);
        unexcused = (TextView) findViewById(R.id.unexcused);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object hackshops = dataSnapshot.child("users/"+mUsername+"/numHackshopAbsencesLeft").getValue();
                Object meetings = dataSnapshot.child("users/"+mUsername+"/numMeetingAbsencesLeft").getValue();
                Object unexcuses = dataSnapshot.child("users/"+mUsername+"/numUnexcusedLeft").getValue();

                Long hackString = (Long) hackshops;
                Long meetingString = (Long) meetings;
                Long unex = (Long) unexcuses;

                hackshop.setText(hackString.toString());
                meeting.setText(meetingString.toString());
                unexcused.setText(unex.toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        hackshop.setText(getNumHackshopAbsences() + "");
//        meeting.setText(getNumMeetingAbsences() + "");

        revealFAB = (RevealFAB) findViewById(R.id.reveal_fab);
        Intent intent2 = new Intent(AbsenceActivity.this, AbsenceRequestActivity.class);
        intent2.putExtra("username", mUsername);
        revealFAB.setIntent(intent2);

        revealFAB.setOnClickListener(new RevealFAB.OnClickListener() {
            @Override
            public void onClick(RevealFAB button, View v) {
                button.startActivityWithAnimation();
            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//                Intent intent = new Intent(AbsenceActivity.this, AbsenceRequestActivity.class);
//                startActivity(intent);
//            }
//        });


        //get # of hackshop absences from server
        //get # of meeting absences from servers
        //set views
    }


    @Override
    protected void onResume() {
        super.onResume();
        revealFAB.onResume();
    }

    private void setupWindowAnimations() {
        Explode exp = new Explode();
        exp.setDuration(1000);
        getWindow().setEnterTransition(exp);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AbsenceActivity.this, MainActivity.class);
        startActivity(intent);
    }


}
